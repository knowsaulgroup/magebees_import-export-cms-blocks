<?php
namespace Magebees\Cmsblocks\Controller\Adminhtml\Exportcmsblocks;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Index extends \Magento\Backend\App\Action
{
    protected $resultPageFactory;

    public function __construct(
        Context $context,
        PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    public function execute()
    {
        $resultPage = $this->resultPageFactory->create();
        $this->_setActiveMenu('Magebees_Cmsblocks::export');
        $resultPage->getConfig()->getTitle()->prepend(__('Import/Export CMS Static Blocks'));
        $resultPage->getConfig()->getTitle()->prepend(__('Export CMS Static Blocks'));
        $resultPage->addBreadcrumb(__('Export CMS Static Blocks'), __('Export CMS Static Blocks'));
        return $resultPage;
    }
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Magebees_Cmsblocks::export');
    }
}
