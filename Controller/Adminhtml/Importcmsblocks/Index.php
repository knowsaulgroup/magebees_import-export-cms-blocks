<?php
namespace Magebees\Cmsblocks\Controller\Adminhtml\Importcmsblocks;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Index extends \Magento\Backend\App\Action
{
    protected $resultPageFactory;

    public function __construct(
        Context $context,
        PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    public function execute()
    {
        $resultPage = $this->resultPageFactory->create();
        $this->_setActiveMenu('Magebees_Cmsblocks::import');
        $resultPage->getConfig()->getTitle()->prepend(__('Import/Export CMS Static Blocks'));
        $resultPage->getConfig()->getTitle()->prepend(__('Import CMS Static Blocks'));
        $resultPage->addBreadcrumb(__('Import CMS Static Blocks'), __('Import CMS Static Blocks'));
        return $resultPage;
    }
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Magebees_Cmsblocks::import');
    }
}
