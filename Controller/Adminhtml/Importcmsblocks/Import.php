<?php
namespace Magebees\Cmsblocks\Controller\Adminhtml\Importcmsblocks;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Filesystem\DirectoryList;

class Import extends \Magento\Backend\App\Action
{
    protected $resultPageFactory;
    protected $mappings = [];
    
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    public function execute()
    {
        $data = $this->getRequest()->getPost();
        $files =  $this->getRequest()->getFiles();
        if (isset($files['filename']['name']) && $files['filename']['name'] != '') {
            try {
                $uploader = $this->_objectManager->create('Magento\MediaStorage\Model\File\Uploader', ['fileId' => 'filename']);
                $allowed_ext_array = ['csv'];
                $uploader->setAllowedExtensions($allowed_ext_array);
                $uploader->setAllowRenameFiles(true);
                $uploader->setFilesDispersion(true);
                $mediaDirectory = $this->_objectManager->get('Magento\Framework\Filesystem')
                    ->getDirectoryRead(DirectoryList::VAR_DIR);
                $result = $uploader->save($mediaDirectory->getAbsolutePath('import/'));
                $path = $mediaDirectory->getAbsolutePath('import');
            } catch (\Exception $e) {
                $this->messageManager->addError(__($e->getMessage()));
                $this->_redirect('*/*/index');
                return;
            }
                    
            $filename = $path.$result['file'];
            $id = fopen($path.$result['file'], 'r');
            $data = fgetcsv($id, filesize($filename));
            if (!$this->mappings) {
                $this->mappings = $data;
            }
         
            $error = [];
            while ($data = fgetcsv($id, filesize($filename))) {
                if ($data[0]) {
                    try {
                        foreach ($data as $key => $value) {
                            $converted_data[$this->mappings[$key]] = addslashes($value);
                        }
                        if (isset($converted_data['stores'])) {
                            if ($converted_data['stores'] == "") {
                                $converted_data['stores'] = [0];
                            }
                            if (!empty($converted_data['identifier'])) {
                                $converted_data['stores'] = explode("|", $converted_data['stores']);
                                $cmsmodel = $this->_objectManager->create('Magento\Cms\Model\Block');
                                $identifier = $converted_data['identifier'];
                                if (in_array(0, $converted_data['stores'])) {
                                    $converted_data['stores'] = [0];
                                }
                                $cmsblockid = $cmsmodel->load($identifier)->getId();
                                if ($cmsblockid) {
                                    $cmsmodel->load($cmsblockid);
                                }
								$converted_data['content'] =  stripslashes($converted_data['content']);
                                $cmsmodel->addData($converted_data);
                                $cmsmodel->save();
                            }
                        } else {
                            $errors = "<div class='message message-error error'><div data-ui-id='messages-message-error'>Please check Your CSV File.</div></div>";
                            $this->getResponse()->representJson($this->_objectManager->get('Magento\Framework\Json\Helper\Data')->jsonEncode($errors));
                            return;
                        }
                    } catch (\Exception $e) {
                        $error[] = "<div class='message message-error error'><div data-ui-id='messages-message-error'>".'Identifier == '.$converted_data['identifier'].' >> '.$e->getMessage()."</div></div>";
                    }
                }
            }
            fclose($id);
        }
            
        if (!empty($error)) {
            $this->getResponse()->representJson($this->_objectManager->get('Magento\Framework\Json\Helper\Data')->jsonEncode($error));
            return;
        }
        
        $result = "<div class='message message-success success'><div data-ui-id='messages-message-success'>CMS static blocks were successfully Imported</div></div>";
        $this->getResponse()->representJson($this->_objectManager->get('Magento\Framework\Json\Helper\Data')->jsonEncode($result));
    }
    
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Magebees_Cmsblocks::import');
    }
}
